package com.smart.shop.shopsmart;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.impl.client.HttpClients;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by gauth_000 on 12-Aug-16.
 */
public class ImageAnalyzer
{
    private HttpClient httpClient;
    private HttpPost postRequest;
    private MultipartEntityBuilder multipartEntityBuilder;

    public ImageAnalyzer() throws URISyntaxException
    {
        this.httpClient = HttpClients.createDefault();

        String url = "https://api.projectoxford.ai/vision/v1.0/analyze?";
        URIBuilder builder = new URIBuilder(url);
        String key = "e6d0da105dd944d0bd797714414d5d09";
        builder.setParameter("subscription-key", key);
        builder.setParameter("visualFeatures", "Tags");

        URI uri = builder.build();
        this.postRequest = new HttpPost(uri);

        this.multipartEntityBuilder = MultipartEntityBuilder.create();
    }

    public String recognize_objects(String photoPath) throws IOException
    {
        File imageFile = new File(photoPath);
        multipartEntityBuilder.addBinaryBody
                (
                        "analyzeImage",
                        imageFile,
                        ContentType.create("image/jpeg"),
                        imageFile.getName()
                );
        postRequest.setEntity(multipartEntityBuilder.build());

        HttpResponse response = httpClient.execute(postRequest);
        HttpEntity entity = response.getEntity();

        return EntityUtils.toString(entity);
    }
}
