package com.smart.shop.shopsmart;

/**
 * Created by Aman on 3/14/2016.
 */
public class DataModel
{

    String name;
    String id_;
    String image;
    public String model;
    public String desc;
    public float rating;
    public String buy;

    public DataModel(String name, String id_, String image, String model, String desc, float rating, String buy)
    {
        this.name = name;
        this.id_ = id_;
        this.image = image;
        this.desc = desc;
        this.model = model;
        this.rating = rating;
        this.buy = buy;
    }

}