package com.smart.shop.shopsmart;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dd.CircularProgressButton;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.DataModelHolder>
{
    ImageLoader imageLoader = ImageLoader.getInstance();

    public static class DataModelHolder extends RecyclerView.ViewHolder
    {
        CardView cv;
        TextView personName;
        TextView personAge;
        TextView desc;
        RatingBar rating;
        CircularProgressButton buy;
        TextView model;
        ImageView personPhoto;
        boolean expanded = false;
        ExpandableRelativeLayout expandableRelativeLayout;

        DataModelHolder(View itemView)
        {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);
            personName = (TextView) itemView.findViewById(R.id.person_name);
            personAge = (TextView) itemView.findViewById(R.id.person_age);
            desc = (TextView) itemView.findViewById(R.id.desc);
            model = (TextView) itemView.findViewById(R.id.model_number);
            rating = (RatingBar) itemView.findViewById(R.id.rating);
            buy = (CircularProgressButton) itemView.findViewById(R.id.btnWithText);
            personPhoto = (ImageView) itemView.findViewById(R.id.person_photo);
            expandableRelativeLayout = (ExpandableRelativeLayout) itemView.findViewById(R.id.expandableLayout1);

            expandableRelativeLayout.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    List<DataModel> persons;

    CustomAdapter(List<DataModel> persons)
    {
        this.persons = persons;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public DataModelHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        DataModelHolder pvh = new DataModelHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final DataModelHolder personViewHolder, final int i)
    {
        personViewHolder.personName.setText(persons.get(i).name);
        personViewHolder.personAge.setText(persons.get(i).id_);
        //TODO: Parse HTML
        personViewHolder.desc.setText(Html.fromHtml(Uri.decode(persons.get(i).desc)));
        personViewHolder.rating.setRating(persons.get(i).rating);
        personViewHolder.model.setText(persons.get(i).model);
        imageLoader.displayImage(persons.get(i).image, personViewHolder.personPhoto);
        personViewHolder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (personViewHolder.expanded == false)
                {
                    personViewHolder.expandableRelativeLayout.expand();
                    personViewHolder.expanded = true;
                } else
                {
                    personViewHolder.expanded = false;
                    personViewHolder.expandableRelativeLayout.collapse();
                }
            }
        });
        personViewHolder.buy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String url
                        = persons.get(i).buy;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                v.getContext().startActivity(browserIntent);
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return persons.size();
    }
}