package com.smart.shop.shopsmart;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class CameraActivity extends AppCompatActivity
{
    private int TAKE_PHOTO_CODE = 0;
    public String photoPath;
    public static final String TAG = "CameraActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/ShopSmart/";
        File newdir = new File(dir);
        newdir.mkdirs();

        photoPath = dir + "photo.jpg";

        File newfile = new File(photoPath);
        try
        {
            newfile.createNewFile();
        } catch (IOException ignored)
        {
        }

        Uri outputFileUri = Uri.fromFile(newfile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK)
        {
            Log.d(TAG, "Pic saved:" + photoPath);
            Utils.reduce_image_size(photoPath);

            Intent intent=new Intent(this, ObjectRecognitionActivity.class);
            intent.putExtra("photoPath", photoPath);
            startActivity(intent);
            finish();
        }
    }
}
