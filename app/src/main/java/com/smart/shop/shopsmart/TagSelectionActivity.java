package com.smart.shop.shopsmart;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import cz.msebera.android.httpclient.Header;


public class TagSelectionActivity extends AppCompatActivity
{
    public static final String TAG = "TagSelectionActivity";
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_selection);

        Random randomGenerator = new Random();
        ArrayList<String> tags = getIntent().getStringArrayListExtra("outputs");
        ArrayList<String> colors = new ArrayList<>();
        colors.add("#ED7D31");
        colors.add("#00B0F0");
        colors.add("#FF0000");
        colors.add("#D0CECE");
        colors.add("#00B050");
        colors.add("#9999FF");
        colors.add("#FF5FC6");
        colors.add("#FFC000");
        colors.add("#7F7F7F");
        colors.add("#4800FF");
        final TagView tagView = (TagView) findViewById(R.id.tag_group);
        for (String detected_tag : tags)
        {
            com.cunoraz.tagview.Tag tag = new Tag(detected_tag);
            tag.isDeletable = true;
            tag.radius = 10f;
            tag.layoutColor = Color.parseColor(colors.get(randomGenerator.nextInt(colors.size())));

            tag.tagTextSize = 25;
            tagView.addTag(tag);
        }

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        if (tagView.getTags().size() > 0)
            fetch_product_list(tagView);
        else
            Toast.makeText(TagSelectionActivity.this, "No results found", Toast.LENGTH_SHORT).show();
        tagView.setOnTagDeleteListener(new TagView.OnTagDeleteListener()
        {
            @Override
            public void onTagDeleted(TagView tagView, com.cunoraz.tagview.Tag tag, int i)
            {
                tagView.remove(i);
                if (tagView.getTags().size() > 0)
                    fetch_product_list(tagView);
                else
                    Toast.makeText(TagSelectionActivity.this, "No results found", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetch_product_list(TagView tagView)
    {
        Toast.makeText(getApplicationContext(), "Fetching results...", Toast.LENGTH_SHORT).show();
        String url1 = "http://api.walmartlabs.com/v1/search?query=";
        String query = "";
        for (Tag t : tagView.getTags())
            query += t.text + "+";
        query = query.substring(0, query.length() - 1);
        String key = "&format=json&apiKey=hz8tf2ehev8554dhjjrtnn82";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        client.get(url1 + query + key, params, new JsonHttpResponseHandler()
                {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                    {
                        // Called when response HTTP status is "200 OK"
                        ArrayList<DataModel> arrayList = new ArrayList<>();
                        try
                        {
                            arrayList = getFromObject(response, 10);
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                        CustomAdapter customAdapter = new CustomAdapter(arrayList);
                        recyclerView.setAdapter(customAdapter);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t)
                    {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                        Log.e(TAG, "Error fetching results");
                    }
                }
        );
    }

    private ArrayList<DataModel> getFromObject(JSONObject jsonObj, int maxNumber) throws JSONException
    {
        JSONArray items = jsonObj.getJSONArray("items");
        ArrayList<DataModel> models = new ArrayList<>();
        for (int i = 0; i < items.length() && i < maxNumber; i++)
        {
            JSONObject current = items.getJSONObject(i);
            float rating = 0;
            String model = "Model number : N/A";
            String shortDescription = "";
            try
            {
                rating = (float) current.getDouble("customerRating");
                model = "Model Number : " + current.getString("modelNumber");
            } catch (Exception ignored)
            {
            }
            models.add(new DataModel(current.getString("name"), "$" + current.getString("salePrice"),
                    current.getString("thumbnailImage"), model, shortDescription,
                    rating, current.getString("productUrl")));
        }
        return models;
    }
}
