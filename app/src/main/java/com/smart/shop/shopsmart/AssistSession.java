package com.smart.shop.shopsmart;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.service.voice.VoiceInteractionSession;

import java.io.File;

/**
 * Created by gauth_000 on 12-Aug-16.
 */
public class AssistSession extends VoiceInteractionSession
{
    private Context context;

    public AssistSession(Context context)
    {
        super(context);
        this.context = context;
    }

    @Override
    public void onHandleScreenshot(Bitmap screenshot)
    {
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/ShopSmart/";
        File newdir = new File(dir);
        newdir.mkdirs();

        String photoPath = dir + "photo.jpg";
        Utils.save_image(photoPath, Utils.scale_image(screenshot));

        Intent intent = new Intent(this.context, ObjectRecognitionActivity.class);
        intent.putExtra("photoPath", photoPath);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

        finish();
    }
}
