package com.smart.shop.shopsmart;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.support.v4.app.ActivityCompat.startActivityForResult;

/**
 * Created by gauth_000 on 12-Aug-16.
 */
public class Utils
{
    public static final int RESULT_CROP=1;


    public static void reduce_image_size(String photoPath)
    {
        Bitmap bitmapImage = BitmapFactory.decodeFile(photoPath);
        save_image(photoPath, scale_image(bitmapImage));
    }

    public static Bitmap scale_image(Bitmap bitmapImage)
    {
        int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
        return Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);
    }

    public static void save_image(String photoPath, Bitmap bitmap)
    {
        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(photoPath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
