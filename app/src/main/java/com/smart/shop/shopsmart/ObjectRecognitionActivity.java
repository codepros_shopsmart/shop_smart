package com.smart.shop.shopsmart;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class ObjectRecognitionActivity extends AppCompatActivity
{
    public static String TAG = "ObjectRecognitionActivity";
    private final int RESULT_CROP = 400;
    private String photoPath;
    private MaterialDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_recognition);


        progressDialog = new MaterialDialog.Builder(this)
                .content("Analyzing")
                .progress(true, 0).build();

        photoPath = getIntent().getStringExtra("photoPath");
        crop_image();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == RESULT_CROP && resultCode == Activity.RESULT_OK)
        {
            progressDialog.show();
            ImageRecognitionTask recognizeObjects = new ImageRecognitionTask(photoPath);
            recognizeObjects.execute();
        }
    }

    private void crop_image()
    {
        try
        {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            File f = new File(photoPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, RESULT_CROP);
        } catch (ActivityNotFoundException anfe)
        {
            String errorMessage = "Your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private class ImageRecognitionTask extends AsyncTask<Void, Void, Void>
    {
        private String jsonResponse;
        private String photoPath;

        public ImageRecognitionTask(String photoPath)
        {
            this.photoPath = photoPath;
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            try
            {
                ImageAnalyzer imageAnalyzer = new ImageAnalyzer();
                jsonResponse = imageAnalyzer.recognize_objects(photoPath);
            } catch (URISyntaxException | IOException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            try
            {
                JSONObject root = new JSONObject(jsonResponse);
                JSONArray tags = root.getJSONArray("tags");
                ArrayList<String> outputs = new ArrayList<>();
                String[] filter_list = new String[]{"indoor", "sitting", "open", "floor", "person", "sport", "holding", "hitting", "player", "cut"};

                for (int i = 0; i < tags.length(); ++i)
                {
                    JSONObject tag = tags.getJSONObject(i);
                    if (!is_present(tag.get("name").toString(), filter_list))
                        outputs.add(tag.get("name").toString());
                }

                Intent intent = new Intent(ObjectRecognitionActivity.this, TagSelectionActivity.class);
                intent.putStringArrayListExtra("outputs", outputs);
                progressDialog.dismiss();
                startActivity(intent);
                finish();
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        private boolean is_present(String tag, String[] filter_list)
        {
            for (String filter : filter_list)
                if (filter.equals(tag))
                    return true;
            return false;
        }
    }
}
