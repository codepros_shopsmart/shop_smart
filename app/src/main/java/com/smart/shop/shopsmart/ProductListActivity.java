package com.smart.shop.shopsmart;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProductListActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        RecyclerView rv = (RecyclerView) findViewById(R.id.my_recycler_view);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(llm);
        JSONObject jsonObj;
        ArrayList<DataModel> arrayList = new ArrayList<>();
        try
        {
            jsonObj = new JSONObject(getIntent().getStringExtra("data"));
            arrayList = getFromObject(jsonObj, 10);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
        CustomAdapter customAdapter = new CustomAdapter(arrayList);
        rv.setAdapter(customAdapter);
    }

    private ArrayList<DataModel> getFromObject(JSONObject jsonObj, int maxNumber) throws JSONException
    {
        JSONArray items = jsonObj.getJSONArray("items");
        ArrayList<DataModel> models = new ArrayList<>();
        for (int i = 0; i < items.length() && i < maxNumber; i++)
        {
            JSONObject current = items.getJSONObject(i);
            models.add(new DataModel(current.getString("name"), current.getString("salePrice") + "$", current.getString("thumbnailImage"),current.getString("brandName"),current.getString("longDescription"),(float)current.getDouble("customerRating"),current.getString("productUrl")));
        }
        return models;
    }
}
